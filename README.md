
# Fella vs Ninja
==============

![DeviantART](http://fc07.deviantart.net/fs71/f/2010/351/9/a/da_link_button_v2_by_rstovall-d352k3z.gif)

A game I created back in school after completing my first Actionscript 3 class. 

You can find a live version here: [Fella vs Ninja](http://pixelworlds.deviantart.com/gallery/28747762#/d3a79k7 "Fella vs Ninja")

You can find my deviantART account here: [Pixelworlds](http://pixelworlds.deviantart.com/ "Pixelworlds")
