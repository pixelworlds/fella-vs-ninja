//Version 2.0.0

package
{
	import com.pixelworlds.facebook.FlashGameToFacebook;
	import com.pixelworlds.ui.Background;
	import com.pixelworlds.ui.BeginGame;
	import com.pixelworlds.ui.GameOver;
	import com.pixelworlds.ui.PlayGrid;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.system.Security;
	import flash.system.SecurityDomain;
	import flash.ui.Mouse;
	
	[SWF(width="850", height="650", frameRate="60")]
	public class fellaVsNinja extends Background
	{
		private var _bg:Background = new Background();
		private	var _gameover:GameOver = new GameOver();
		private var _curScore:int = 0;
		private var _bestScore:int = 0;
		private var _playGrid:PlayGrid;
		private var _fg2fb:FlashGameToFacebook;
					
		public function fellaVsNinja()
		{
			/*Security.loadPolicyFile("xml/crossdomain.xml");*/
			
			init();
		}
		private function init():void
		{
			addChild(_bg);
	
			var beginGame:BeginGame = new BeginGame();
			beginGame.x = 325;
			beginGame.y = 325;
			addChild(beginGame);
						
			beginGame.addEventListener(MouseEvent.CLICK, onClickBegin);
		}
		private function onClickBegin(e:MouseEvent):void
		{
			removeChild(BeginGame(e.currentTarget));
			startGame();
		}
		private function startGame():void 
		{
			Mouse.hide();
			
			_curScore = 0;
			_bg.setScore(_curScore, _bestScore);
			
			_playGrid = new PlayGrid();
			_playGrid.addEventListener(PlayGrid.SCORE_UP, onScoreUp);
			_playGrid.addEventListener(PlayGrid.GAME_OVER, onGameOver);
			_playGrid.x = 80;
			_playGrid.y = 75;
			addChild(_playGrid);
		}
		private function onGameOver(e:Event):void
		{
			Mouse.show();
			
			removeChild(_playGrid);
			var gameOver:GameOver = new GameOver();
			gameOver.addEventListener(MouseEvent.CLICK, onTryAgain);
			gameOver.x = 325;
			gameOver.y = 325;
			addChild(gameOver);
			
			/*_fg2fb = new FlashGameToFacebook(_curScore.toString());
			_fg2fb.x = 255;
			_fg2fb.y = 420;
			addChild(_fg2fb);*/
		}
		private function onScoreUp(e:Event):void
		{
			_curScore ++;
			if(_curScore > _bestScore)
			{
				_bestScore = _curScore;
			}
			_bg.setScore(_curScore, _bestScore);
		}
		private function onTryAgain(e:MouseEvent):void
		{
			_playGrid.removeEventListener(PlayGrid.SCORE_UP, onScoreUp);
			_playGrid.removeEventListener(PlayGrid.GAME_OVER, onGameOver);
			removeChild(GameOver(e.currentTarget));
			startGame();
		}
	}
}