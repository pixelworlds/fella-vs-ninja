package com.pixelworlds.facebook
{
	import com.facebook.graph.Facebook;
	
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	import flash.utils.Timer;
	
	import libs.FacebookButtonBase;
	
	public class FlashGameToFacebook extends FacebookButtonBase
	{
		protected static const APP_ID:String = "101083876652949";
		
		private var _userName:String;
		private var _score:String;
		private var _timer:Timer = new Timer(1000);
		
		public function FlashGameToFacebook(score:String)
		{
			super();
			_score = score;
			
			Facebook.init(APP_ID);
			
			this.buttonMode = true;
			this.mouseChildren = false;
			this.stop();
			this.addEventListener(MouseEvent.CLICK, doPost);
		}
	
		private function doPost(e:MouseEvent):void
		{
			var urlRequest:URLRequest = new URLRequest("https://www.facebook.com/dialog/feed?");
			
			var urlVars:URLVariables = new URLVariables();
			urlVars.app_id = APP_ID;
			urlVars.scope = "publish_stream";
			urlVars.link = "https://www.facebook.com/apps/application.php?id=101083876652949";
			urlVars.picture = "https://fbcdn-photos-a.akamaihd.net/photos-ak-snc1/v27562/141/101083876652949/app_1_101083876652949_2789.gif";
			urlVars.name = "Fella vs Ninja";
			urlVars.caption = "rstovall.deviantart.com";
			/*urlVars.display = "popup";*/
			urlVars.description = "Death by ninja! Score: " + _score;
			urlVars.redirect_uri = "https://www.facebook.com/apps/application.php?id=101083876652949";
		
			urlRequest.data = urlVars;
		
			if(ExternalInterface.available)
			{
				ExternalInterface.call("window.open", urlRequest.url + urlRequest.data, "win");
			}

			
			
			
		}
	}
}