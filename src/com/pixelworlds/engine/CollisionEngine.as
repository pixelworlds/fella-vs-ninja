package com.pixelworlds.engine
{
	import com.coreyoneil.collision.CDK;
	import com.pixelworlds.ui.Ninja;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	
	public class CollisionEngine extends CDK
	{
		public function CollisionEngine(hand:Sprite, points:Sprite)
		{
			super();
			
			addItem(hand);
			addItem(points);
		}
		public function checkCollisions():void
		{
			clearArrays();
			
			var NUM_OBJS:uint = objectArray.length;
			var item1:DisplayObject = DisplayObject(objectArray[0]), item2:DisplayObject;
			for(var i:uint = 1; i < NUM_OBJS; i++)
			{
				item2 = DisplayObject(objectArray[i]);
				
				if(item1.hitTestObject(item2))
				{
					if((item2.width * item2.height) > (item1.width * item1.height))
					{
						objectCheckArray.push([item1,item2])
					}
					else
					{
						objectCheckArray.push([item2,item1]);
					}
				}
			}
			
			NUM_OBJS = objectCheckArray.length;
			for(i = 0; i < NUM_OBJS; i++)
			{
				findCollisions(DisplayObject(objectCheckArray[i][0]), DisplayObject(objectCheckArray[i][1]));
			}
		}
	}
}