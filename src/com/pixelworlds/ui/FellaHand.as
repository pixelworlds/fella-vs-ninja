package com.pixelworlds.ui
{
	import libs.FellaHandBase;
	
	public class FellaHand extends FellaHandBase
	{
		public function FellaHand()
		{
			super();
			this.name = "hand";
		}
		public function updatePosition(x:Number, y:Number):void
		{
			this.x = x;
			this.y = y;
			
			if(this.x >= 480)
			{
				this.x = 480;
			}
			else if (this.x <= 20)
			{
				this.x = 20;
			}
			else if(this.y >= 472)
			{
				this.y = 472;
			}
			else if(this.y <= 28)
			{
				this.y = 28;
			}
		}
	}
}