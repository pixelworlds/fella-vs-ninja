package com.pixelworlds.ui
{
	import libs.PointsBase;
	
	public class Points extends PointsBase
	{
		public function Points()
		{
			super();
			this.name = "points";
		}
		public function relocate():void
		{
			this.x = 20 + Math.random()* (500-this.width);
			this.y = 20 + Math.random()* (500-this.height);
		}
	}
}