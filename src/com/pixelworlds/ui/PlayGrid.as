package com.pixelworlds.ui
{
	import com.coreyoneil.collision.CDK;
	import com.pixelworlds.engine.CollisionEngine;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class PlayGrid extends Sprite
	{
		public static const SCORE_UP:String = "score up";
		public static const GAME_OVER:String = "game over";
		
		private var _hand:FellaHand = new FellaHand();
		private var _points:Points = new Points();
		private var _ninjasArray:Array = [];
		private var _ninja:Ninja;
		private var _collisionEngine:CollisionEngine;
		
		public function PlayGrid()
		{
			super();
			
			this.addChild(_points);
			this.addChild(_hand);
			
			_points.relocate();
			
			_collisionEngine = new CollisionEngine(_hand, _points);
			_collisionEngine.addEventListener(CDK.POINT_COLLISION, onPointCollision);
			_collisionEngine.addEventListener(CDK.NINJA_COLLISION, onNinjaCollision);

			addNinja();
			
			this.addEventListener(Event.ENTER_FRAME, updateAll);
		}
		private function addNinja():void
		{
			_ninja  = new Ninja();
			_ninjasArray.push(_ninja);
			addChild(_ninja);
			
			_collisionEngine.addItem(_ninja);
		}
		private function updateAll(e:Event):void
		{
			_hand.updatePosition(mouseX, mouseY);
			for each(var n:Ninja in _ninjasArray)
			{
				n.moveNinja();
			}
			_collisionEngine.checkCollisions();
		}
		private function onPointCollision(e:Event):void
		{
			dispatchEvent(new Event(PlayGrid.SCORE_UP));
			addNinja();
			_points.relocate();
		}
		private function onNinjaCollision(e:Event):void
		{
			this.removeEventListener(Event.ENTER_FRAME, updateAll);
			_collisionEngine.removeEventListener(CDK.POINT_COLLISION, onPointCollision);
			_collisionEngine.removeEventListener(CDK.NINJA_COLLISION, onNinjaCollision);
			
			while(_ninjasArray.length > 0)
			{
				removeChild(_ninjasArray.pop());
			}
			removeChild(_hand);
			dispatchEvent(new Event(PlayGrid.GAME_OVER));
		}
	}
}