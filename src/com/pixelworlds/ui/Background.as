package com.pixelworlds.ui
{
	import libs.BackgroundBase;
	
	
	public class Background extends BackgroundBase
	{
		private	var _scoreBoard1:ScoreBoard;
		private var _scoreBoard2:ScoreBoard;
		
		public function Background()
		{
			super();
			
			var extraBamboo:ExtraBamboo = new ExtraBamboo();
			extraBamboo.x = 40;
			extraBamboo.y = 150;			
			addChild(extraBamboo);
			
			var bambooBorder:BambooBorder = new BambooBorder();
			bambooBorder.y = 325;
			bambooBorder.x = 330;
			addChild(bambooBorder);
			
			_scoreBoard1 = new ScoreBoard();
			_scoreBoard1.scaleX = _scoreBoard1.scaleY = .75;
			_scoreBoard1.tfLabel.text = "Current Score";
			_scoreBoard1.x = 720;
			_scoreBoard1.y = 545;
			addChild(_scoreBoard1);
			
			var leaves:Leaves = new Leaves();
			leaves.x = 785;
			leaves.y = 385;
			addChild(leaves);
			
			_scoreBoard2 = new ScoreBoard();
			_scoreBoard2.scaleX = _scoreBoard2.scaleY = .75;
			_scoreBoard2.tfLabel.text = "Best Score";
			_scoreBoard2.x = 720;
			_scoreBoard2.y = 445;
			addChild(_scoreBoard2);
			
			var ninjaScroll:Scroll = new Scroll();
			ninjaScroll.x = 720;
			ninjaScroll.y = 200;
			addChild(ninjaScroll);
		}
		public function setScore(curScore:int, bestScore:int):void
		{
			_scoreBoard1.tfScore.text = curScore.toString();
			_scoreBoard2.tfScore.text = bestScore.toString();
		}
	}
}