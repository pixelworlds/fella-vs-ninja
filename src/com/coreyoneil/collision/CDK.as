﻿package com.coreyoneil.collision
{
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.errors.EOFError;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	
	public class CDK extends EventDispatcher
	{
		protected var objectArray:Array;
		protected var objectCheckArray:Array;
		protected var objectCollisionArray:Array;
		
		public static const POINT_COLLISION:String = "point collision";
		public static const NINJA_COLLISION:String = "ninja collision";
		
		private var colorExclusionArray:Array;
		
		private var bmd1:BitmapData;
		private var bmd2:BitmapData;
		private var bmdResample:BitmapData;
		
		private var pixels1:ByteArray;
		private var pixels2:ByteArray;
		
		private var rect1:Rectangle;
		private var rect2:Rectangle;
		
		private var transMatrix1:Matrix;
		private var transMatrix2:Matrix;
		
		private var colorTransform1:ColorTransform;
		private var colorTransform2:ColorTransform;
		
		private var item1Registration:Point;
		private var item2Registration:Point;
		
		private var _alphaThreshold:Number;
		private var _returnAngle:Boolean;
		private var _returnAngleType:String;
		
		private var _numChildren:uint;

		public function CDK():void 
		{	
			init();
		}
		private function init():void
		{			
			objectCheckArray = [];
			objectCollisionArray = [];
			objectArray = [];
			colorExclusionArray = [];
			
			_alphaThreshold = 0;
			_returnAngle = true;
			_returnAngleType = "RADIANS";
		}	
		public function addItem(obj:DisplayObject):void 
		{
			if(obj is DisplayObject)
			{
				objectArray.push(obj);
			}
			else
			{
				throw new Error("Cannot add item: " + obj + " - item must be a Display Object.");
			}
		}
		protected function clearArrays():void
		{
			objectCheckArray = [];
			objectCollisionArray = [];
		}
		
		protected function findCollisions(item1:DisplayObject, item2:DisplayObject):void
		{
			var item1xDiff:Number, item1yDiff:Number;

			colorTransform1 = item1.transform.colorTransform;
			colorTransform2 = item2.transform.colorTransform;
			
			item1Registration = new Point();
			item2Registration = new Point();

			item1Registration = item1.localToGlobal(item1Registration);
			item2Registration = item2.localToGlobal(item2Registration);
			
			bmd1 = new BitmapData(item1.width, item1.height, true, 0x00FFFFFF);  
			bmd2 = new BitmapData(item1.width, item1.height, true, 0x00FFFFFF);
			
			transMatrix1 = item1.transform.matrix;
			
			var currentObj:DisplayObject = item1;
			while(currentObj.parent != null)
			{
				transMatrix1.concat(currentObj.parent.transform.matrix);
				currentObj = currentObj.parent;
			}
			
			rect1 = item1.getBounds(currentObj);
			if(item1 != currentObj)
			{
				rect1.x += currentObj.x;
				rect1.y += currentObj.y;
			}
			
			transMatrix1.tx = item1xDiff = (item1Registration.x - rect1.left);
			transMatrix1.ty = item1yDiff = (item1Registration.y - rect1.top);
			
			transMatrix2 = item2.transform.matrix;
			
			currentObj = item2;
			while(currentObj.parent != null)
			{
				transMatrix2.concat(currentObj.parent.transform.matrix);
				currentObj = currentObj.parent;
			}
			
			transMatrix2.tx = (item2Registration.x - rect1.left);
			transMatrix2.ty = (item2Registration.y - rect1.top);
			
			bmd1.draw(item1, transMatrix1, colorTransform1, null, null, true);
			bmd2.draw(item2, transMatrix2, colorTransform2, null, null, true);
			
			pixels1 = bmd1.getPixels(new Rectangle(0, 0, bmd1.width, bmd1.height));
			pixels2 = bmd2.getPixels(new Rectangle(0, 0, bmd1.width, bmd1.height));	
			
			var k:uint = 0, value1:uint = 0, value2:uint = 0, collisionPoint:Number = -1, overlap:Boolean = false, overlapping:Array = [];
			var locY:Number, locX:Number, locStage:Point, hasColors:int = colorExclusionArray.length;
			
			pixels1.position = 0;
			pixels2.position = 0;
			
			var pixelLength:int = pixels1.length;
			while(k < pixelLength)
			{
				k = pixels1.position;
				
				try
				{
					value1 = pixels1.readUnsignedInt();
					value2 = pixels2.readUnsignedInt();
				}
				catch(e:EOFError)
				{
					break;
				}
				
				var alpha1:uint = value1 >> 24 & 0xFF, alpha2:uint = value2 >> 24 & 0xFF;
				
				if(alpha1 > _alphaThreshold && alpha2 > _alphaThreshold)
				{	
					var colorFlag:Boolean = false;
					if(hasColors)
					{
						var red1:uint = value1 >> 16 & 0xFF, red2:uint = value2 >> 16 & 0xFF, green1:uint = value1 >> 8 & 0xFF, green2:uint = value2 >> 8 & 0xFF, blue1:uint = value1 & 0xFF, blue2:uint = value2 & 0xFF;
						
						var colorObj:Object, aPlus:uint, aMinus:uint, rPlus:uint, rMinus:uint, gPlus:uint, gMinus:uint, bPlus:uint, bMinus:uint, item1Flags:uint, item2Flags:uint;
						
						for(var n:uint = 0; n < hasColors; n++)
						{
							colorObj = Object(colorExclusionArray[n]);
							
							item1Flags = 0;
							item2Flags = 0;
							if((blue1 >= colorObj.bMinus) && (blue1 <= colorObj.bPlus))
							{
								item1Flags++;
							}
							if((blue2 >= colorObj.bMinus) && (blue2 <= colorObj.bPlus))
							{
								item2Flags++;
							}
							if((green1 >= colorObj.gMinus) && (green1 <= colorObj.gPlus))
							{
								item1Flags++;
							}
							if((green2 >= colorObj.gMinus) && (green2 <= colorObj.gPlus))
							{
								item2Flags++;
							}
							if((red1 >= colorObj.rMinus) && (red1 <= colorObj.rPlus))
							{
								item1Flags++;
							}
							if((red2 >= colorObj.rMinus) && (red2 <= colorObj.rPlus))
							{
								item2Flags++;
							}
							if((alpha1 >= colorObj.aMinus) && (alpha1 <= colorObj.aPlus))
							{
								item1Flags++;
							}
							if((alpha2 >= colorObj.aMinus) && (alpha2 <= colorObj.aPlus))
							{
								item2Flags++;
							}
							
							if((item1Flags == 4) || (item2Flags == 4)) colorFlag = true;
						}
					}
					
					if(!colorFlag)
					{
						overlap = true;

						collisionPoint = k >> 2;
						
						locY = collisionPoint / bmd1.width, locX = collisionPoint % bmd1.width;
			
						locY -= item1yDiff;
						locX -= item1xDiff;
						
						locStage = item1.localToGlobal(new Point(locX, locY));
						overlapping.push(locStage);
					}
				}
			}	
			if(overlap)
			{
				if(item1.name == "points")
				{
					dispatchEvent(new Event(CDK.POINT_COLLISION));
				}
				else if(item1.name == "ninja")
				{
					dispatchEvent(new Event(CDK.NINJA_COLLISION));
				}
			}
		}
	}
}